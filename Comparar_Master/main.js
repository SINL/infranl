/*
 * I N I C I A   V U E
 * ----------------------------------------------------------------------
 */
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

//prueba drios
import axios from 'axios'

Vue.config.productionTip = false

//prueba drios
axios.defaults.baseURL = 'https://localhost:44325/'

new Vue({
  router,
  store,
  render: h => h(App),
  
  beforeCreate () {
    // console.log(store.getters.getProjectsReady,"popop");

    //24/09/2020 Se comenta el GetRecordPackages y el getProjectPackages 
    //El gelProjectPackages hace que marque error al cargar la pagina en projects
    store.dispatch("getReleases")
    .then( 
      () => {
        store.commit('setReleasesReady');
        // console.log("Hola ","setReleasesReady");
        // store.dispatch("getRecordPackages")
        // .then( () => {
        //   store.commit('setRecordPackagesReady');
        //   console.log("Hola ","setRecordPackagesReady");
          store.dispatch('getProjects') 
          .then( () => {
            store.commit('setProjectsReady');
            // console.log("Hola ","setProjectsReady");
            // store.dispatch('getProjectPackages') 
            // .then( () => {
            //   store.commit('setProjectPackagesReady');
            //   console.log("Hola ","setProjectPackagesReady");
              store.state.Projects.projects.forEach( project => store.dispatch('createExtendedProject', project));
              console.log(store.getters.getProjectsReady,"projectsReady");
            // });
          // });
        });
      } 
      );

      

  },

  mounted(){
    
    //console.log("Hola ","getProjects");
    // habilita los releases de NL
    // let contractReleases = store.dispatch("getReleases")
    //                             .then( () => store.commit('setReleasesReady') ),
    // // habilita los record packages del INAI
    //       recordPackages = store.dispatch("getRecordPackages")
    //                             .then( () => store.commit('setRecordPackagesReady') ),
    // // habilita los proyectos de obra de NL
    //             projects = store.dispatch('getProjects') 
    //                             .then( () => store.commit('setProjectsReady') ),
    //      projectPackages = store.dispatch('getProjectPackages') 
    //                             .then( () => store.commit('setProjectPackagesReady') );

    // store.dispatch("getReleases")
    // .then( 
    //   () => {
    //     store.commit('setReleasesReady');
    //     console.log("Hola ","setReleasesReady");
    //     store.dispatch("getRecordPackages")
    //     .then( () => {
    //       store.commit('setRecordPackagesReady');
    //       console.log("Hola ","setRecordPackagesReady");
    //       store.dispatch('getProjects') 
    //       .then( () => {
    //         store.commit('setProjectsReady');
    //         console.log("Hola ","setProjectsReady");
    //         store.dispatch('getProjectPackages') 
    //         .then( () => {
    //           store.commit('setProjectPackagesReady');
    //           console.log("Hola ","setProjectPackagesReady");
    //           store.state.Projects.projects.forEach( project => store.dispatch('createExtendedProject', project));
    //         });
    //       });
    //     });
    //   } 
    //   );

                                
    // console.log("Hola ","createExtendedProject");
    // Promise.all([contractReleases, recordPackages, projects, projectPackages]).then( () => {

    //   store.state.Projects.projects.forEach( project => store.dispatch('createExtendedProject', project));
      
    //   // aquí hace algo cuando toda la info está lista. 
    //   // por ahora todo carga desde el inicio, pero es
    //   // posible modificar eso en el store.
    // });
  },


}).$mount('#app')


  // mounted(){
  //   // habilita los releases de NL
  //   let contractReleases = store.dispatch("getReleases")
  //                               .then( () => store.commit('setReleasesReady') ),
  //   // habilita los record packages del INAI
  //         // recordPackages = store.dispatch("getRecordPackages")
  //         //                       .then( () => store.commit('setRecordPackagesReady') ),
  //   // habilita los proyectos de obra de NL
  //               projects = store.dispatch('getProjects') 
  //                               .then( () => store.commit('setProjectsReady') );
  //       //  projectPackages = store.dispatch('getProjectPackages') 
  //       //                         .then( () => store.commit('setProjectPackagesReady') );

  //   // Promise.all([contractReleases, recordPackages, projects, projectPackages]).then( () => {
  //     Promise.all([contractReleases, projects]).then( () => {
  //     //console.log(store.state);
  //     store.state.Projects.projects.forEach( project => store.dispatch('createExtendedProject', project));
      
  //     // aquí hace algo cuando toda la info está lista. 
  //     // por ahora todo carga desde el inicio, pero es
  //     // posible modificar eso en el store.
  //   });
  // }
// }).$mount('#app')
